"""
Make snapshot

{"Tasks": {"total": 440, "running": 1, "sleeping": 354, "stopped": 1, "zombie": 0},
"%CPU": {"user": 14.4, "system": 2.2, "idle": 82.7},
"KiB Mem": {"total": 16280636, "free": 335140, "used": 11621308},
"KiB Swap": {"total": 16280636, "free": 335140, "used": 11621308},
"Timestamp": 1624400255}
"""
import json
import time
import psutil
import argparse
from psutil import Process
from psutil import swap_memory as sm
from psutil import cpu_times as cpu_time
from psutil import virtual_memory as vm


def main():
    """Snapshot tool."""
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", help="Interval between snapshots in seconds", type=int, default=5)
    parser.add_argument("-f", help="Output file name", default="snapshot.json")
    parser.add_argument("-n", help="Quantity of snapshot to output", default=30)

    class Object:
        def toJson(self):
            return json.dumps(self, default=lambda o: o.__dict__,
                              sort_keys=True, indent=4)

    # print(psutil.Process(pid))

    util = Object()
    util.swap = Object()
    util.swap.used = sm().used
    util.swap.total = sm().total
    util.swap.percent = sm().percent

    pid = psutil.pids()
    for i in range(len(pid)):
        # print(pid[i])
        util.proccess = Object()
        util.proccess.pid = Process(pid[i]).pid
        util.proccess.name = Process(pid[i]).name
        util.proccess.status = Process(pid[i]).status

    util.virtual_memory = Object()
    util.virtual_memory.total = vm().total
    util.virtual_memory.available = vm().available
    util.virtual_memory.used = vm().used
    util.virtual_memory.free = sm().free
    util.virtual_memory.percent = vm().percent

    util.cputimes = Object()
    util.cputimes.user = cpu_time().user
    util.cputimes.system = cpu_time().system
    util.cputimes.idle = cpu_time().idle
    util.cputimes.interrupt = cpu_time().interrupt
    util.cputimes.dpc = cpu_time().dpc

    class Print():
        def util():
            print(util.toJson())

    args = parser.parse_args()
    with open(args.f, "a") as file:
        file.write(util.toJson())
        # json.dump(file)
        for i in range(10):
            Print.util()
            time.sleep(10)
            i += i


if __name__ == "__main__":
    main()
